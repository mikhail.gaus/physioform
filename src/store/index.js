import Vue from "vue";
import Vuex from "vuex";
import qData from "../assets/template.json";
import defaultPdf from "../assets/defaultPdfDataUrl";
import pdfMake from "../../node_modules/pdfmake/build/pdfmake";
import pdfFonts from "../../node_modules/pdfmake/build/vfs_fonts";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

Vue.use(Vuex);

export default new Vuex.Store({
  //the state shows all projects the global variables that can be reached from the store.
  state: {
    showQuestionList: true,
    questions: qData,
    selectedQuestions: [],
    iframesrc: defaultPdf.defaultPdf,
    checkboxMap: qData.question.map(() => {
      return false;
    })
  },
  mutations: {
    //updates the selectedQuestions array (adds a object).
    ADD_NEW_QUESTION_OBJECT: (state, payload) => {
      state.selectedQuestions.push(payload);
    },
    //updates the selectedQuestions array (recudes a object).
    REDUCE_ADDED_QUESTION_OBJECT: (state, payload) => {
      const qs = state.selectedQuestions.indexOf(payload);
      if (qs > -1) {
        state.selectedQuestions.splice(qs, 1);
      }
    },
    IMPORT_FROM_JSON: (state, payload) => {
      for(let question of payload) {
        state.selectedQuestions.push(question);
      }
    },
    SET_CHECKBOX_VALUE_BY_INDEX: (state, {index, value}) => {
      let newArr = [...state.checkboxMap];
      newArr[index] = value;
      state.checkboxMap = newArr;
    },
    //generates the iframe to show the pdf preview and creates the pdf's content.
    UPDATE_IFRAME_SRC: state => {
      let doc = {
        content: [
          {
            text: "Patientenfragebogen",
            alignment: "center",
            fontSize: 19,
            bold: true,
            underline: true
          },
          {
            text: "\n" + "Patientenstammdaten:",
            fontSize: 15,
            bold: true,
            color: "#58A8A0"
          },
          {
            columns: [
              {
                text:
                  "\n" +
                  "_____________________________________________________",
                fontSize: 10,
                color: "#58A8A9"
              },
              {
                text:
                  "\n" +
                  "_____________________________________________________",
                fontSize: 10,
                color: "#58A8A9"
              }
            ]
          },
          {
            columns: [
              { text: "Name" + "\n", fontSize: 10 },
              { text: "Vorname" + "\n", fontSize: 10 }
            ]
          },
          {
            columns: [
              {
                text:
                  "\n" +
                  "_____________________________________________________",
                fontSize: 10,
                color: "#58A8A9"
              },
              {
                text:
                  "\n" +
                  "_____________________________________________________",
                fontSize: 10,
                color: "#58A8A9"
              }
            ]
          },
          {
            columns: [
              { text: "Geburtsdatum" + "\n", fontSize: 10 },
              { text: "Geburtsort" + "\n", fontSize: 10 }
            ]
          },
          {
            columns: [
              {
                text:
                  "\n" +
                  "_____________________________________________________",
                fontSize: 10,
                color: "#58A8A9"
              },
              {
                text:
                  "\n" +
                  "_____________________________________________________",
                fontSize: 10,
                color: "#58A8A9"
              }
            ]
          },
          {
            columns: [
              { text: "Straße und Hausnummer" + "\n", fontSize: 10 },
              { text: "Wohnort" + "\n", fontSize: 10 }
            ]
          },
          {
            columns: [
              {
                text:
                  "\n" +
                  "_____________________________________________________",
                fontSize: 10,
                color: "#58A8A9"
              },
              {
                text:
                  "\n" +
                  "_____________________________________________________",
                fontSize: 10,
                color: "#58A8A9"
              }
            ]
          },
          {
            columns: [
              { text: "Telefon" + "\n", fontSize: 10 },
              { text: "Hausarzt" + "\n", fontSize: 10 }
            ]
          },
          {
            text: "\n" + "Bitte beantworten Sie folgende Fragen:",
            fontSize: 15,
            bold: true,
            color: "#58A8A0"
          },
          { text: "\n" }
        ]
      };
      for (let selectedQuestion of state.selectedQuestions) {
        if (selectedQuestion.options === "none") {
          doc.content.push(
            { text: selectedQuestion.text },
            {
              text:
                "\n" +
                "____________________________________________________________________________________________",
              color: "#58A8A0"
            },
            { text: "Antwort" + "\n" + "\n" + " ", fontSize: 10 }
          );
        } else {
          doc.content.push({ text: selectedQuestion.text + "\n" + " " });
          for (let option in selectedQuestion.options) {
            doc.content.push({
              text: [{ text: "o", fontSize: 20 }, { text: " " + option }]
            });
          }
          doc.content.push({ text: "\n" + " " });
        }
      }
      const pdfobj = pdfMake.createPdf(doc);
      pdfobj.getDataUrl(function(encodedString) {
        state.iframesrc = encodedString;
      });
    },
    //updates selectedQuestions array (adds new self created object).
    ADD_NEW_SELF_CREATED_QUESTION_OBJECT: (state, payload) => {
      state.questions.question.push(payload);
    },
    //updates showQuestionsList and changes its value.
    SHOW_QUESTION_LIST_OBJECT: state => {
      state.showQuestionList = !state.showQuestionList;
    }
  },
  actions: {
    //call the mutation ADD_NEW_QUESTION_OBJECT to add a new question.
    addNewQuestionObject: (context, payload) => {
      context.commit("ADD_NEW_QUESTION_OBJECT", payload);
      context.dispatch("updateIframesrc");
    },
    //call the mutation REDUCE_ADDED_QUESTION_OBJECT to reduce a selected question.
    deleteQuestionObject: (context, payload) => {
      context.commit("REDUCE_ADDED_QUESTION_OBJECT", payload);
      context.dispatch("updateIframesrc");
    },
    //call the mutation UPDATE_IFRAME_SRC to update the pdf iframe.
    updateIframesrc: context => {
      context.commit("UPDATE_IFRAME_SRC");
    },
    //call the mutation ADD_NEW_SELF_CREATED_QUESTION_OBJECT to add a new self creared question object.
    addNewSelfCreatedQuestionObject: (context, payload) => {
      context.commit("ADD_NEW_SELF_CREATED_QUESTION_OBJECT", payload);
    },
    //call the mutation SHOW_QUESTION_LIST_OBJECT to change the value of the boolean showQuestionList.
    showQuestionListObject: context => {
      context.commit("SHOW_QUESTION_LIST_OBJECT");
    },
    downloadJSON: context => {
      let dataStr =
        "data:text/json;charset=utf-8," +
        encodeURIComponent(
          JSON.stringify({ question: context.state.selectedQuestions })
        );
      let downloadAnchorNode = document.createElement("a");
      downloadAnchorNode.setAttribute("href", dataStr);
      downloadAnchorNode.setAttribute("download", "fragebogen.json");
      document.body.appendChild(downloadAnchorNode); // required for firefox
      downloadAnchorNode.click();
      downloadAnchorNode.remove();
    },
    downloadPDF: context => {
      let downloadAnchorNode = document.createElement("a");
      downloadAnchorNode.setAttribute("href", context.state.iframesrc);
      downloadAnchorNode.setAttribute("download", "fragebogen.pdf");
      document.body.appendChild(downloadAnchorNode); // required for firefox
      downloadAnchorNode.click();
      downloadAnchorNode.remove();
    },
    importFromJson: (context, payload) => {
      if (context.state.selectedQuestions.length !== 0) {
        this.$router.go(0);
      }
      context.commit("IMPORT_FROM_JSON", payload);
      for(let question of payload) {
        let questionIdx = parseInt(question.number)-1;
        if(questionIdx > context.state.questions.question.length-1) {
          context.commit('ADD_NEW_SELF_CREATED_QUESTION_OBJECT', question);
        }
        context.commit('SET_CHECKBOX_VALUE_BY_INDEX', {index: questionIdx, value: true});
      }
      context.dispatch("updateIframesrc");
    },
    setCheckboxValueByIndex: (context, {index, value}) => {
      context.commit('SET_CHECKBOX_VALUE_BY_INDEX', {index, value});
    }
  },
  getters: {
    //this gets the all the selectedQuestions.
    getSelectedQuestions: state => {
      return state.selectedQuestions;
    },
    //this gets the boolean showQuestionList to find out its value.
    getShowQuestionList: state => {
      return state.showQuestionList;
    },

    getCheckboxValueByIndex: state => index => {
      return state.checkboxMap[index];
    }
  }
});
