import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Privacy from "../views/Privacy.vue";
import Impressum from "../views/Impressum";

Vue.use(VueRouter);

//creates routes to home, privacy and impressum to show them on different pages and give
//each one of them a url.

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/privacy",
    name: "Privacy",
    component: Privacy
  },
  {
    path: "/impressum",
    name: "Impressum",
    component: Impressum
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
