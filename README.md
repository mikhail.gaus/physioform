# PhysioForm
## Description 
Physioform is an application, that will help physiotherapists make forms for documentation faster and better.
Powered by Public Health Faculty of University of Applied Sciences Mittelhessen.

## Installation 
After cloning the repository run:

`npm install`

Run the application in development mode:

`npm run serve`

### Compiles and minifies for production

`npm run build`

